
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN

const byte address[6] = "00001";

struct package
  {
    float temperatura = 0;
    float x = 0;
    float y = 0;
    float z = 0;
  };

typedef struct package Package;
Package data;
void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
}

void loop() {
  if (radio.available()) {
    radio.read(&data, sizeof(data));
    Serial.print(data.x);
    Serial.print(',');
    Serial.print(data.y);
    Serial.print(',');
    Serial.print(data.z);
    Serial.print(',');
    Serial.println(data.temperatura);
  }
}
