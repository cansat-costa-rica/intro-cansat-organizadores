#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <SparkFun_ADXL345.h> 
 
ADXL345 adxl = ADXL345();
RF24 radio(7, 8); // CE, CSN

const byte address[6] = "00001";

struct package
  {
    float temperatura = 10;
    float x = 0;
    float y = 0;
    float z = 0;
  };

  typedef struct package Package;
  Package data;

void setup() {
  adxl.powerOn();            
  adxl.setRangeSetting(16);
  Serial.begin(9600);
  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}

void loop() {
   int x, y, z;
   adxl.readAccel(&x, &y, &z);
   data.x=x;
   data.y=y;
   data.z=z;  
   //Serial.print(data.x);
   radio.write(&data, sizeof(data));
   delay(200);
}
