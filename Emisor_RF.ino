#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN

const byte address[6] = "00001";

struct package
  {
    float temperatura = 10;
    float x = 0;
    float y = 0;
    float z = 0;
  };

  typedef struct package Package;
  Package data;

void setup() {
  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}

void loop() {
  radio.write(&data, sizeof(data));
  delay(200);
}
